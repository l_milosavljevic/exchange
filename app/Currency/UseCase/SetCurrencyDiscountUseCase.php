<?php

namespace App\Currency\UseCase;

use App\Currency\Domain\Contract\CurrencyRepositoryContract;
use App\Currency\Infrastructure\UI\Http\Request\SetCurrencyDiscountRequest;
use App\Shared\Library\UseCase\UseCase;

class SetCurrencyDiscountUseCase extends UseCase
{
    /**
     * @param CurrencyRepositoryContract $currencyRepository
     */
    public function __construct(private CurrencyRepositoryContract $currencyRepository)
    {
    }

    /**
     * @param SetCurrencyDiscountRequest $request
     * @return void
     */
    public function execute(SetCurrencyDiscountRequest $request): void
    {
        $this->currencyRepository->updateDiscount(
            $request->get('currencyId'),
            $request->get('discount'),
            $request->get('applyDiscount')
        );
    }
}
