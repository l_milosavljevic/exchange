<?php

namespace App\Currency\UseCase;

use App\Currency\Domain\Contract\CurrencyRepositoryContract;
use App\Currency\Infrastructure\UI\Http\Transformer\CurrencyTransformer;
use App\Shared\Infrastructure\Exception\InvalidTransformerException;
use App\Shared\Library\UseCase\UseCase;

class GetCurrenciesUseCase extends UseCase
{
    /**
     * @param CurrencyRepositoryContract $currencyRepository
     */
    public function __construct(private CurrencyRepositoryContract $currencyRepository)
    {
    }

    /**
     * @return array
     * @throws InvalidTransformerException
     */
    public function execute(): array
    {
        $currencies = $this->currencyRepository->findAll();

        return $this->transformCollection($currencies, CurrencyTransformer::class);
    }
}
