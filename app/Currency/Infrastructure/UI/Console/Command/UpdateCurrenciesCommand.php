<?php

namespace App\Currency\Infrastructure\UI\Console\Command;

use App\Currency\Domain\Contract\CurrencyRepositoryContract;
use App\Currency\Domain\Model\Currency;
use Illuminate\Console\Command;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class UpdateCurrenciesCommand extends Command
{
    public function __construct(private CurrencyRepositoryContract $currencyRepositoryContract)
    {
        parent::__construct();
    }

    /**
     * @var string
     */
    protected $name = "app:currencies-update";

    /**
     * @var string
     */
    protected $description = "Update currencies table with new exchange rates";

    public function handle(): void
    {
        $apiUrl = env('CURRENCY_LAYER_API_URL');
        $apiKey = env('CURRENCY_LAYER_API_KEY');
        $currencies = array_keys(Currency::CURRENCY_NAMES);
        $urlQuery = http_build_query([
            'source' => Currency::CURRENCY_SHORT_USD,
            'currencies' => implode(',', $currencies)
        ]);

        $response = Http::withHeaders([
            "Content-Type: text/plain",
            'apikey' => $apiKey
        ])->get($apiUrl . '?' . $urlQuery);

        if ($response->status() != Response::HTTP_OK) {
            $this->error('Something went wrong! Try again later.');
        }

        $responseData = json_decode($response->body(), true);

        foreach ($responseData['quotes'] as $name => $rate) {
            $this->currencyRepositoryContract->updateRates(
                str_replace(Currency::CURRENCY_SHORT_USD, '', $name),
                $rate
            );
        }

        $this->info('We did it!');
    }
}
