<?php

namespace App\Currency\Infrastructure\UI\Http\Transformer;

use App\Currency\Domain\Model\Currency;
use League\Fractal\TransformerAbstract;

class CurrencyTransformer extends TransformerAbstract
{
    /**
     * @param Currency $currency
     * @return array
     */
    public function transform(Currency $currency): array
    {
        return [
            "id" => $currency->id(),
            "name" => $currency->name(),
            "shortName" => $currency->shortName(),
            'exchangeRate' => $currency->exchangeRate(),
            'surchargePercent' => $currency->surchargePercent(),
            'discount' => $currency->discount(),
            'applyDiscount' => $currency->applyDiscount()
        ];
    }
}
