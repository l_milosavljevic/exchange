<?php

namespace App\Currency\Infrastructure\UI\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class SetCurrencyDiscountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'currencyId' => 'required|integer|numeric',
            'discount' => 'required|numeric',
            'applyDiscount' => 'required|boolean'
        ];
    }
}
