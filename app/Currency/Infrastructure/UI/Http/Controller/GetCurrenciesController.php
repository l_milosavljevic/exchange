<?php

namespace App\Currency\Infrastructure\UI\Http\Controller;

use App\Currency\UseCase\GetCurrenciesUseCase;
use App\Shared\Infrastructure\Exception\InvalidTransformerException;
use App\Shared\Infrastructure\UI\Http\Controller\Controller;
use App\Shared\Library\UseCase\ReadUseCaseResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class GetCurrenciesController extends Controller
{
    /**
     * @param GetCurrenciesUseCase $useCase
     */
    public function __construct(private GetCurrenciesUseCase $useCase)
    {
    }

    /**
     * @return JsonResponse
     * @throws InvalidTransformerException
     */
    public function __invoke(): JsonResponse
    {
        return (new ReadUseCaseResponse())->response($this->useCase->execute());
    }
}
