<?php

namespace App\Currency\Infrastructure\UI\Http\Controller;

use App\Currency\Infrastructure\UI\Http\Request\SetCurrencyDiscountRequest;
use App\Currency\UseCase\SetCurrencyDiscountUseCase;
use App\Shared\Infrastructure\UI\Http\Controller\Controller;
use App\Shared\Library\UseCase\NoContentUseCaseResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class SetCurrencyDiscountController extends Controller
{
    /**
     * @param SetCurrencyDiscountUseCase $useCase
     */
    public function __construct(private SetCurrencyDiscountUseCase $useCase)
    {
    }

    /**
     * @param SetCurrencyDiscountRequest $request
     * @return JsonResponse
     */
    public function __invoke(SetCurrencyDiscountRequest $request): JsonResponse
    {
        $this->useCase->execute($request);

        return (new NoContentUseCaseResponse())->response([]);
    }
}
