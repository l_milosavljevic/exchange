<?php

namespace App\Currency\Infrastructure\Repository;

use App\Currency\Domain\Contract\CurrencyRepositoryContract;
use App\Currency\Domain\Model\Currency;
use App\Currency\Infrastructure\Eloquent\Currency as EloquentCurrencyModel;
use App\Currency\Infrastructure\Exception\CurrencyNotFoundException;
use App\Currency\Library\Collection\CurrencyCollection;

class EloquentCurrencyRepository implements CurrencyRepositoryContract
{
    /**
     * @param EloquentCurrencyModel $eloquentModel
     */
    public function __construct(private EloquentCurrencyModel $eloquentModel)
    {
    }

    /**
     * @param int $id
     * @return Currency
     */
    public function getOne(int $id): Currency
    {
        if (!$currency = $this->eloquentModel->find($id)) {
            throw CurrencyNotFoundException::byId($id);
        }

        return Currency::create(
            $currency->id,
            $currency->short_name,
            $currency->exchange_rate,
            $currency->surcharge_percent,
            $currency->discount,
            $currency->apply_discount
        );
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $currencies = $this->eloquentModel->all();

        return (new CurrencyCollection($currencies))->resource();
    }

    /**
     * @param int $id
     * @param float $discount
     * @param bool $applyDiscount
     */
    public function updateDiscount(int $id, float $discount, bool $applyDiscount): void
    {
        if (!$currency = $this->eloquentModel->find($id)) {
            throw CurrencyNotFoundException::byId($id);
        }

        $currency->update([
            'discount' => $discount,
            'apply_discount' => $applyDiscount
        ]);
    }

    public function updateRates(string $shortName, float $exchangeRate): void
    {
        if (!$currency = $this->eloquentModel->where('short_name', $shortName)->first()) {
            throw CurrencyNotFoundException::byShortName($shortName);
        }

        $currency->update([
            'exchange_rate' => $exchangeRate
        ]);
    }
}
