<?php

namespace App\Currency\Infrastructure\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = [
        'name',
        'short_name',
        'exchange_rate',
        'surcharge_percent',
        'discount',
        'apply_discount'
    ];
}
