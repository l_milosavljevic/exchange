<?php

namespace App\Currency\Infrastructure\Exception;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class CurrencyNotFoundException extends ModelNotFoundException
{
    /**
     * @param int $id
     * @return CurrencyNotFoundException
     */
    public static function byId(int $id): self
    {
        $msg = \sprintf('Currency with id: (%d) does not exist.', $id);

        return new self($msg);
    }

    /**
     * @param string $shortName
     * @return CurrencyNotFoundException
     */
    public static function byShortName(string $shortName): self
    {
        $msg = \sprintf('Currency with short name: (%s) does not exist.', $shortName);

        return new self($msg);
    }
}
