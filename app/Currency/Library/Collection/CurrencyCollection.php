<?php

namespace App\Currency\Library\Collection;

use App\Currency\Domain\Model\Currency;
use Illuminate\Database\Eloquent\Collection;

class CurrencyCollection
{
    public function __construct(private Collection $currencies)
    {
    }

    public function resource(): array
    {
        return $this->currencies->map(function ($currency) {
            return Currency::create(
                $currency->id,
                $currency->short_name,
                $currency->exchange_rate,
                $currency->surcharge_percent,
                $currency->discount,
                $currency->apply_discount
            );
        })->toArray();
    }
}
