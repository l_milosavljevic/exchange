<?php

namespace App\Currency\Domain\Contract;

use App\Currency\Domain\Model\Currency;

interface CurrencyRepositoryContract
{
    public function getOne(int $id): Currency;

    public function findAll(): array;

    public function updateDiscount(int $id, float $discount, bool $applyDiscount): void;

    public function updateRates(string $shortName, float $exchangeRate): void;
}
