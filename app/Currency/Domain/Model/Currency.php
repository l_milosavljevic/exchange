<?php

namespace App\Currency\Domain\Model;

use App\Currency\Infrastructure\Exception\CurrencyNotFoundException;
use App\Shared\Domain\Model\Domain;

class Currency extends Domain
{
    public const CURRENCY_SHORT_USD = 'USD';
    public const CURRENCY_SHORT_JPY = 'JPY';
    public const CURRENCY_SHORT_GBP = 'GBP';
    public const CURRENCY_SHORT_EUR = 'EUR';

    public const CURRENCY_NAMES = [
        self::CURRENCY_SHORT_JPY => 'Japanese Yen',
        self::CURRENCY_SHORT_GBP => 'British Pound',
        self::CURRENCY_SHORT_EUR => 'Euro'
    ];

    private function __construct(
        private int $id,
        private string $name,
        private string $shortName,
        private float $exchangeRate,
        private float $surchargePercent,
        private float $discount,
        private bool $applyDiscount
    ) {
    }

    public static function create(
        int $id,
        string $shortName,
        float $exchangeRate,
        float $surchargePercent,
        float $discount,
        bool $applyDiscount,
    ): self {
        return new self(
            $id,
            self::getCurrencyNameByShortName($shortName),
            $shortName,
            $exchangeRate,
            $surchargePercent,
            $discount,
            $applyDiscount
        );
    }

    public static function getCurrencyNameByShortName(string $shortName): string
    {
        if (!array_key_exists($shortName, self::CURRENCY_NAMES)) {
            throw CurrencyNotFoundException::byShortName($shortName);
        }

        return self::CURRENCY_NAMES[$shortName];
    }

    public function calculateDiscountPercentage(): float
    {
        if ($this->applyDiscount()) {
            return $this->discount();
        }

        return 0;
    }


    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function shortName(): string
    {
        return $this->shortName;
    }

    public function exchangeRate(): float
    {
        return $this->exchangeRate;
    }

    public function surchargePercent(): float
    {
        return $this->surchargePercent;
    }

    public function discount(): float
    {
        return $this->discount;
    }

    public function applyDiscount(): bool
    {
        return $this->applyDiscount;
    }
}
