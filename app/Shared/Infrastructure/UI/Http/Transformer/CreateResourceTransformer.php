<?php

namespace App\Shared\Infrastructure\UI\Http\Transformer;

use App\Shared\Domain\Model\Domain;
use League\Fractal\TransformerAbstract;

class CreateResourceTransformer extends TransformerAbstract
{
    /**
     * @param Domain $model
     * @return array
     */
    public function transform(Domain $model): array
    {
        return [
            "id" => $model->id()
        ];
    }
}
