<?php

namespace App\Shared\Infrastructure\Provider;

use App\Currency\Infrastructure\Eloquent\Currency;
use App\Currency\Infrastructure\Repository\EloquentCurrencyRepository;
use App\Order\Infrastructure\Eloquent\Order;
use App\Order\Infrastructure\Repository\EloquentOrderRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Currency\Domain\Contract\CurrencyRepositoryContract', function($app) {
            return new EloquentCurrencyRepository(new Currency);
        });

        $this->app->bind('App\Order\Domain\Contract\OrderRepositoryContract', function($app) {
            return new EloquentOrderRepository(new Order);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
