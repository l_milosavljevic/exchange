<?php

namespace App\Shared\Library\UseCase;

use Symfony\Component\HttpFoundation\Response;

class ReadUseCaseResponse extends UseCaseResponse
{
    protected int $httpCode = Response::HTTP_OK;
}
