<?php

namespace App\Shared\Library\UseCase;

use Symfony\Component\HttpFoundation\Response;

class CreateUseCaseResponse extends UseCaseResponse
{
    protected int $httpCode = Response::HTTP_CREATED;
}
