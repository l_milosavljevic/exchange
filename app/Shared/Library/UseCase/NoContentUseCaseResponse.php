<?php

namespace App\Shared\Library\UseCase;

use Symfony\Component\HttpFoundation\Response;

class NoContentUseCaseResponse extends UseCaseResponse
{
    protected int $httpCode = Response::HTTP_NO_CONTENT;
}
