<?php

namespace App\Shared\Library\UseCase;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class UseCaseResponse
{
    protected int $httpCode = Response::HTTP_OK;

    /**
     * @param array $response
     * @return JsonResponse
     */
    public function response(array $response): JsonResponse
    {
        return new JsonResponse($response, $this->httpCode);
    }
}
