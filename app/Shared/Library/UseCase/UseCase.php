<?php

namespace App\Shared\Library\UseCase;

use App\Shared\Infrastructure\Exception\InvalidTransformerException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\DataArraySerializer;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;

class UseCase
{
    /**
     * @param LengthAwarePaginator $paginator
     * @param $transformerClassName
     * @return array
     * @throws InvalidTransformerException
     */
    public function transformWithPagination(LengthAwarePaginator $paginator, $transformerClassName): array
    {
        $transformer = $this->checkTransformer($transformerClassName);

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());

        $collection = $paginator->getCollection();
        $resource = new Collection($collection, $transformer);
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return $manager->createData($resource)->toArray();
    }

    /**
     * @param $model
     * @param $transformerClassName
     * @param $includes
     * @return array
     * @throws InvalidTransformerException
     */
    public function transformItem($model, $transformerClassName, $includes = null): array
    {
        $transformer = $this->checkTransformer($transformerClassName);
        $manager = new Manager();
        if ($includes) {
            $manager->parseIncludes($includes);
        }
        $resource = new Item($model, $transformer);

        return $manager->createData($resource)->toArray();
    }

    /**
     * @param $collection
     * @param $transformerClassName
     * @return array
     * @throws InvalidTransformerException
     */
    public function transformCollection($collection, $transformerClassName): array
    {
        $transformer = $this->checkTransformer($transformerClassName);
        $manager = new Manager();
        $resource = new Collection($collection, $transformer);

        return $manager->createData($resource)->toArray();
    }

    /**
     * @param $transformerClassName
     * @return TransformerAbstract
     * @throws InvalidTransformerException
     */
    private function checkTransformer($transformerClassName): TransformerAbstract
    {
        if ($transformerClassName instanceof TransformerAbstract) {
            // check, if we have provided a respective TRANSFORMER class
            $transformer = $transformerClassName;
        } else {
            // of if we just passed the classname
            $transformer = new $transformerClassName();
        }

        if (!($transformer instanceof TransformerAbstract)) {
            throw new InvalidTransformerException();
        }

        return $transformer;
    }
}
