<?php

namespace App\Order\Infrastructure\Repository;

use App\Order\Domain\Model\Order;
use App\Order\Infrastructure\Eloquent\Order as EloquentOrderModel;
use App\Order\Domain\Contract\OrderRepositoryContract;
use App\Order\Infrastructure\Exception\OrderNotFoundException;
use Ramsey\Uuid\UuidInterface;

class EloquentOrderRepository implements OrderRepositoryContract
{
    /**
     * @param EloquentOrderModel $eloquentModel
     */
    public function __construct(private EloquentOrderModel $eloquentModel)
    {
    }

    /**
     * @param UuidInterface $uuid
     * @return Order
     */
    public function getOne(UuidInterface $uuid): Order
    {
        if (!$order = $this->eloquentModel->find($uuid)) {
            throw OrderNotFoundException::byId($uuid);
        }

        return Order::create(
            $order->currency_purchased,
            $order->currency_exchange_rate,
            $order->surcharge_percent,
            $order->currency_purchased_amount,
            $order->discount_percentage,
            $order->id,
            $order->created_at
        );
    }

    /**
     * @param Order $order
     */
    public function persist(Order $order): void
    {
        $eloquentModel = new EloquentOrderModel;
        $eloquentModel->id = $order->id();
        $eloquentModel->currency_purchased = $order->currencyPurchased();
        $eloquentModel->currency_exchange_rate = $order->currencyExchangeRate();
        $eloquentModel->surcharge_percentage = $order->surchargePercent();
        $eloquentModel->surcharge_amount = $order->surchargeAmount();
        $eloquentModel->currency_purchased_amount = $order->currencyPurchasedAmount();
        $eloquentModel->paid_amount_dollars = $order->paidAmountDollars();
        $eloquentModel->discount_percentage = $order->discountPercentage();
        $eloquentModel->discount_amount = $order->discountAmount();
        $eloquentModel->created_at = $order->createdAt();
        $eloquentModel->save();
    }
}
