<?php

namespace App\Order\Infrastructure\Exception;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class OrderNotFoundException extends NotFoundResourceException
{
    /**
     * @param UuidInterface $uuid
     * @return OrderNotFoundException
     */
    public static function byId(UuidInterface $uuid): OrderNotFoundException
    {
        $msg = \sprintf('Order with id: (%s) does not exist.', $uuid);

        return new self($msg);
    }
}
