<?php

namespace App\Order\Infrastructure\UI\Http\Transformer;

use App\Order\Domain\Model\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    /**
     * @param Order $order
     * @return array
     */
    public function transform(Order $order): array
    {
        return [
            "currencyPurchased" => $order->currencyPurchased(),
            "currencyExchangeRate" => $order->currencyExchangeRate(),
            'surchargePercent' => $order->surchargePercent(),
            'surchargeAmount' => $order->surchargeAmount(),
            'discountPercentage' => $order->discountPercentage(),
            'discountAmount' => $order->discountAmount(),
            'currencyPurchasedAmount' => $order->currencyPurchasedAmount(),
            'paidAmountDollars' => $order->paidAmountDollars(),
            'total' => $order->paidAmountDollars() + $order->surchargeAmount() - $order->discountAmount()
        ];
    }
}
