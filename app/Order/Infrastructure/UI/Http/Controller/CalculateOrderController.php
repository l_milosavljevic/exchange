<?php

namespace App\Order\Infrastructure\UI\Http\Controller;

use App\Order\Infrastructure\UI\Http\Request\CreateOrderRequest;
use App\Order\UseCase\CalculateOrderUseCase;
use App\Shared\Infrastructure\Exception\InvalidTransformerException;
use App\Shared\Infrastructure\UI\Http\Controller\Controller;
use App\Shared\Library\UseCase\ReadUseCaseResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class CalculateOrderController extends Controller
{
    /**
     * @param CalculateOrderUseCase $useCase
     */
    public function __construct(private CalculateOrderUseCase $useCase)
    {
    }

    /**
     * @param CreateOrderRequest $request
     * @return JsonResponse
     * @throws InvalidTransformerException
     */
    public function __invoke(CreateOrderRequest $request): JsonResponse
    {
        return (new ReadUseCaseResponse())->response($this->useCase->execute($request));
    }
}
