<?php

namespace App\Order\Infrastructure\UI\Http\Controller;

use App\Order\Infrastructure\UI\Http\Request\CreateOrderRequest;
use App\Order\UseCase\CreateOrderUseCase;
use App\Shared\Infrastructure\Exception\InvalidTransformerException;
use App\Shared\Infrastructure\UI\Http\Controller\Controller;
use App\Shared\Library\UseCase\CreateUseCaseResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class CreateOrderController extends Controller
{
    /**
     * @param CreateOrderUseCase $useCase
     */
    public function __construct(private CreateOrderUseCase $useCase)
    {
    }

    /**
     * @param CreateOrderRequest $request
     * @return JsonResponse
     * @throws InvalidTransformerException
     */
    public function __invoke(CreateOrderRequest $request): JsonResponse
    {
        return (new CreateUseCaseResponse())->response($this->useCase->execute($request));
    }
}
