<?php

namespace App\Order\Infrastructure\Eloquent\Listener;

use App\Currency\Domain\Model\Currency;
use App\Order\Infrastructure\Eloquent\Event\OrderCreatedEvent;
use App\Order\Infrastructure\Mail\OrderCreated;
use Illuminate\Support\Facades\Mail;

class OrderCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {}

    /**
     * Handle the event.
     *
     * @param OrderCreatedEvent $event
     * @param string $mailAddress
     * @return void
     */
    public function handle(OrderCreatedEvent $event, string $mailAddress = 'test@test.com')
    {
        $order = $event->getOrder();

        if ($order->currency_purchased === Currency::CURRENCY_SHORT_GBP) {
            Mail::to($mailAddress)->send(new OrderCreated($order));
        }
    }
}
