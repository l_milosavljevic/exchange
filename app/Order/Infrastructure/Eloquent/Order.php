<?php

namespace App\Order\Infrastructure\Eloquent;

use App\Order\Infrastructure\Eloquent\Event\OrderCreatedEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    use Notifiable;

    protected $fillable = [
        'id',
        'currency_purchased',
        'currency_exchange_rate',
        'surcharge_percentage',
        'surcharge_amount',
        'currency_purchased_amount',
        'paid_amount_dollars',
        'discount_percentage',
        'discount_amount'
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => OrderCreatedEvent::class,
    ];
}
