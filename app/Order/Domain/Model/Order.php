<?php

namespace App\Order\Domain\Model;

use App\Shared\Domain\Model\Domain;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Order extends Domain
{
    private function __construct(
        private UuidInterface $id,
        private string $currencyPurchased,
        private float $currencyExchangeRate,
        private float $surchargePercent,
        private float $currencyPurchasedAmount,
        private float $discountPercentage,
        private \DateTimeImmutable $createdAt,
        private float $surchargeAmount = 0,
        private float $paidAmountDollars = 0,
        private float $discountAmount = 0
    ) {
    }

    public static function create(
        string $currencyPurchased,
        float $currencyExchangeRate,
        float $surchargePercent,
        float $currencyPurchasedAmount,
        float $discountPercentage,
        ?UuidInterface $id = null,
        ?\DateTimeImmutable $createdAt = null
    ): self {
        $order = new self(
            $id ?? Uuid::uuid4(),
            $currencyPurchased,
            $currencyExchangeRate,
            $surchargePercent,
            $currencyPurchasedAmount,
            $discountPercentage,
            $createdAt ?? new \DateTimeImmutable()
        );

        $order->setSurchargeAmount()
            ->setDiscountAmount()
            ->setPaidAmountDollars()
        ;

        return $order;
    }

    private function setSurchargeAmount(): self
    {
        $this->surchargeAmount =
            $this->currencyPurchasedAmount() / $this->currencyExchangeRate() * $this->surchargePercent() / 100
        ;

        return $this;
    }

    private function setDiscountAmount(): self
    {
        $this->discountAmount =
            $this->currencyPurchasedAmount() / $this->currencyExchangeRate() * $this->discountPercentage() / 100
        ;

        return $this;
    }

    private function setPaidAmountDollars(): self
    {
        $this->paidAmountDollars =
            $this->currencyPurchasedAmount() / $this->currencyExchangeRate() +
            $this->surchargeAmount() - $this->discountAmount()
        ;

        return $this;
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function currencyPurchased(): string
    {
        return $this->currencyPurchased;
    }

    public function currencyExchangeRate(): float
    {
        return $this->currencyExchangeRate;
    }

    public function surchargePercent(): float
    {
        return $this->surchargePercent;
    }

    public function surchargeAmount(): float
    {
        return $this->surchargeAmount;
    }

    public function currencyPurchasedAmount(): float
    {
        return $this->currencyPurchasedAmount;
    }

    public function paidAmountDollars(): float
    {
        return $this->paidAmountDollars;
    }

    public function discountPercentage(): float
    {
        return $this->discountPercentage;
    }

    public function discountAmount(): float
    {
        return $this->discountAmount;
    }

    public function createdAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }
}
