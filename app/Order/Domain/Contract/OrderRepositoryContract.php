<?php

namespace App\Order\Domain\Contract;

use App\Order\Domain\Model\Order;
use Ramsey\Uuid\UuidInterface;

interface OrderRepositoryContract
{
    public function getOne(UuidInterface $uuid): Order;

    public function persist(Order $order): void;
}
