<?php

namespace App\Order\UseCase;

use App\Currency\Domain\Contract\CurrencyRepositoryContract;
use App\Currency\Infrastructure\Exception\CurrencyNotFoundException;
use App\Order\Domain\Contract\OrderRepositoryContract;
use App\Order\Domain\Model\Order;
use App\Order\Infrastructure\UI\Http\Request\CreateOrderRequest;
use App\Shared\Infrastructure\Exception\InvalidTransformerException;
use App\Shared\Infrastructure\UI\Http\Transformer\CreateResourceTransformer;
use App\Shared\Library\UseCase\UseCase;

class CreateOrderUseCase extends UseCase
{
    /**
     * @param OrderRepositoryContract $orderRepository
     * @param CurrencyRepositoryContract $currencyRepository
     */
    public function __construct(
        private OrderRepositoryContract $orderRepository,
        private CurrencyRepositoryContract $currencyRepository
    ) {
    }

    /**
     * @param CreateOrderRequest $request
     * @return array
     * @throws InvalidTransformerException|CurrencyNotFoundException
     */
    public function execute(CreateOrderRequest $request): array
    {
        $currency = $this->currencyRepository->getOne($request->get('currencyId'));

        $order = Order::create(
            $currency->shortName(),
            $currency->exchangeRate(),
            $currency->surchargePercent(),
            $request->get('amount'),
            $currency->calculateDiscountPercentage()
        );

        $this->orderRepository->persist($order);

        return $this->transformItem($order, CreateResourceTransformer::class);
    }
}
