<?php

namespace App\Order\UseCase;

use App\Currency\Domain\Contract\CurrencyRepositoryContract;
use App\Currency\Infrastructure\Exception\CurrencyNotFoundException;
use App\Order\Domain\Model\Order;
use App\Order\Infrastructure\UI\Http\Request\CreateOrderRequest;
use App\Order\Infrastructure\UI\Http\Transformer\OrderTransformer;
use App\Shared\Infrastructure\Exception\InvalidTransformerException;
use App\Shared\Library\UseCase\UseCase;

class CalculateOrderUseCase extends UseCase
{
    /**
     * @param CurrencyRepositoryContract $currencyRepository
     */
    public function __construct(
        private CurrencyRepositoryContract $currencyRepository
    ) {
    }

    /**
     * @param CreateOrderRequest $request
     * @return array
     * @throws InvalidTransformerException|CurrencyNotFoundException
     */
    public function execute(CreateOrderRequest $request): array
    {
        $currency = $this->currencyRepository->getOne($request->get('currencyId'));

        $order = Order::create(
            $currency->shortName(),
            $currency->exchangeRate(),
            $currency->surchargePercent(),
            $request->get('amount'),
            $currency->calculateDiscountPercentage()
        );

        return $this->transformItem($order, OrderTransformer::class);
    }
}
