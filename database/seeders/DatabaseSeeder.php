<?php

namespace Database\Seeders;

use App\Currency\Domain\Model\Currency;
use App\Currency\Infrastructure\Eloquent\Currency as EloquentCurrency;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        EloquentCurrency::create([
            'name' => Currency::CURRENCY_NAMES[Currency::CURRENCY_SHORT_EUR],
            'short_name' => Currency::CURRENCY_SHORT_EUR,
            'exchange_rate' => 0.884872,
            'surcharge_percent' => 5,
            'discount' => 2,
            'apply_discount' => true
        ]);

        EloquentCurrency::create([
            'name' => Currency::CURRENCY_NAMES[Currency::CURRENCY_SHORT_JPY],
            'short_name' => Currency::CURRENCY_SHORT_JPY,
            'exchange_rate' => 107.17,
            'surcharge_percent' => 7.5,
            'apply_discount' => false
        ]);

        EloquentCurrency::create([
            'name' => Currency::CURRENCY_NAMES[Currency::CURRENCY_SHORT_GBP],
            'short_name' => Currency::CURRENCY_SHORT_GBP,
            'exchange_rate' => 0.711178,
            'surcharge_percent' => 5,
            'apply_discount' => false
        ]);
    }
}
