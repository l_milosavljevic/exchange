<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('currency_purchased');
            $table->decimal('currency_exchange_rate', 9, 6);
            $table->decimal('surcharge_percentage');
            $table->decimal('surcharge_amount');
            $table->decimal('currency_purchased_amount');
            $table->decimal('paid_amount_dollars');
            $table->decimal('discount_percentage');
            $table->decimal('discount_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
