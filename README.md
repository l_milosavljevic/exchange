Exchange API Installation
============================================

The Exchange API platform use Docker to quickly deploy a development environment. The docker-compose.yml is configured to deploy 3 containers :

* Nginx [front]
* Php 8.1 [engine]
* Mysql 8.0 [db]

INSTALLATION
------------

### SetUp
Run this command to start with Exchange API
* `make init` - This command will build docker, install composer, create database, run migrations scripts and compile FE files
* After this, you can navigate to http://localhost:81/ in your browser. There you should see the app running.
* `make update_currencies` - This command will update currency rates in the database which was one of the requirements of this task.
* `make test` - This one is for running tests. I added couple of functional test.

For stopping all containers, you should run `make stop`. Additionally, you can check Makefile for other useful scripts.
