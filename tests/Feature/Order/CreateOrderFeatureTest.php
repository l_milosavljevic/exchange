<?php

namespace Tests\Feature\Order;

use App\Currency\Infrastructure\Exception\CurrencyNotFoundException;
use App\Order\Infrastructure\Eloquent\Order;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Tests\TestCase;

class CreateOrderFeatureTest extends TestCase
{
    use DatabaseTransactions;

    public function test_calculate_order_currency_not_found_exception(): void
    {
        $currencyId = 555;

        $this->expectException(CurrencyNotFoundException::class);
        $this->expectExceptionMessage('Currency with id: (' . $currencyId . ') does not exist.');
        $this->withoutExceptionHandling();

        $this->post(route('orders.create'), [
            'currencyId' => 555,
            'amount' => 100,
        ]);
    }

    public function test_calculate_order_success(): void
    {
        $response = $this->post(route('orders.create'), [
            'currencyId' => 1,
            'amount' => 100,
        ]);

        $response->assertCreated();

        $data = json_decode($response->getContent(), true);

        $uuid = $data['data']['id'];
        $this->assertArrayHasKey('data', $data);
        $this->assertInstanceOf(UuidInterface::class, Uuid::fromString($uuid));
        $this->assertModelExists(Order::find($uuid));
    }
}
