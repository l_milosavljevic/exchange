<?php

namespace Tests\Feature\Order;

use App\Currency\Infrastructure\Exception\CurrencyNotFoundException;
use Tests\TestCase;

class CalculateOrderFeatureTest extends TestCase
{
    public function test_calculate_order_currency_not_found_exception(): void
    {
        $currencyId = 555;

        $this->expectException(CurrencyNotFoundException::class);
        $this->expectExceptionMessage('Currency with id: (' . $currencyId . ') does not exist.');
        $this->withoutExceptionHandling();

        $this->post(route('orders.calculate'), [
            'currencyId' => 555,
            'amount' => 100,
        ]);
    }

    public function test_calculate_order_success(): void
    {
        $response = $this->post(route('orders.calculate'), [
            'currencyId' => 1,
            'amount' => 100,
        ]);

        $response->assertOk();

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('data', $data);
    }
}
