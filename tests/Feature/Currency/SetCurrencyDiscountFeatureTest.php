<?php

namespace Tests\Feature\Currency;

use App\Currency\Infrastructure\Eloquent\Currency;
use App\Currency\Infrastructure\Exception\CurrencyNotFoundException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class SetCurrencyDiscountFeatureTest extends TestCase
{
    use DatabaseTransactions;

    public function test_set_currency_discount_currency_not_found_exception(): void
    {
        $currencyId = 555;

        $this->expectException(CurrencyNotFoundException::class);
        $this->expectExceptionMessage('Currency with id: (' . $currencyId . ') does not exist.');
        $this->withoutExceptionHandling();

        $this->patch(route('currencies.set-discount'), [
            'currencyId' => 555,
            'discount' => 100,
            'applyDiscount' => true
        ]);
    }

    public function test_set_currency_discount_currency_success(): void
    {
        $id = 1;
        $discount = 99;
        $delta = 0.00001;

        $currency = Currency::find($id);
        $this->assertNotEqualsWithDelta($discount, $currency->discount, $delta);

        $response = $this->patch(route('currencies.set-discount'), [
            'currencyId' => $id,
            'discount' => $discount,
            'applyDiscount' => true
        ]);

        $response->assertNoContent();
        $currency = Currency::find(1);
        $this->assertEqualsWithDelta($discount, $currency->discount, $delta);
    }
}
