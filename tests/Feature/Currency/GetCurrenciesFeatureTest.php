<?php

namespace Tests\Feature\Currency;

use Tests\TestCase;

class GetCurrenciesFeatureTest extends TestCase
{
    public function test_list_currencies_success(): void
    {
        $response = $this->get(route('currencies.list'));
        $response->assertOk();

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('data', $data);
    }
}
