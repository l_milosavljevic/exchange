<?php

use App\Currency\Infrastructure\UI\Http\Controller\GetCurrenciesController;
use App\Currency\Infrastructure\UI\Http\Controller\SetCurrencyDiscountController;
use App\Order\Infrastructure\UI\Http\Controller\CalculateOrderController;
use App\Order\Infrastructure\UI\Http\Controller\CreateOrderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/currencies', GetCurrenciesController::class)->name('currencies.list');
Route::patch('/currencies/set-discount', SetCurrencyDiscountController::class)->name('currencies.set-discount');

Route::post('/orders/create', CreateOrderController::class)->name('orders.create');
Route::post('/orders/calculate', CalculateOrderController::class)->name('orders.calculate');
