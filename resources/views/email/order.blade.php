<table>
    <tr>
        <th>Currency</th>
        <th>Exchange Rate</th>
        <th>Surcharge (%)</th>
        <th>Surcharge ($)</th>
        <th>Currency Purchased Amount</th>
        <th>Paid Amount ($)</th>
        <th>Discount (%)</th>
        <th>Discount ($)</th>
    </tr>
    <tr>
        <td>{{ $order->currency_purchased }}</td>
        <td>{{ $order->currency_exchange_rate }}</td>
        <td>{{ $order->surcharge_percentage }}</td>
        <td>{{ $order->surcharge_amount }}</td>
        <td>{{ $order->currency_purchased_amount }}</td>
        <td>{{ $order->paid_amount_dollars }}</td>
        <td>{{ $order->discount_percentage }}</td>
        <td>{{ $order->discount_amount }}</td>
    </tr>
</table>
