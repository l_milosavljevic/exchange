<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Exchange</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <!-- Styles -->
    </head>
    <body>
        <div class="container text-center">
            <div class="row">
                <div class="col col-6 offset-3">
                    <p class="text-uppercase fs-1 text-dark-emphasis">Exchange rate list</p>
                    <ul class="list-group js-rate-list"></ul>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col col-6 offset-3">
                    <div class="input-group">
                        <div class="alert alert-danger d-none col-12 js-error-message" role="alert"></div>
                        <input type="text" class="form-control js-currency-value" autofocus>
                        <select class="form-select js-select"></select>
                    </div>

                    <button type="button" class="btn btn-primary mt-5 js-calculate">Calculate</button>
                </div>
            </div>
            <div class="row mt-5 justify-content-center">
                <div class="d-none js-calculations">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Currency</th>
                            <th scope="col">Exchange Rate</th>
                            <th scope="col">Surcharge (%)</th>
                            <th scope="col">Surcharge ($)</th>
                            <th scope="col">Discount (%)</th>
                            <th scope="col">Discount ($)</th>
                            <th scope="col">Currency Amount</th>
                            <th scope="col">Total Price Net ($)</th>
                            <th scope="col">Total Price (Net Price + Surcharge - Discount) ($)</th>
                        </tr>
                        </thead>
                        <tbody><tr class="js-table-calculation-results"></tr></tbody>
                    </table>

                    <button type="button" class="btn btn-primary w-auto mt-5 js-purchase">Purchase</button>
                </div>

                <div class="alert alert-success mt-4 d-none col-12 js-success-message" role="alert"></div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
    </body>
</html>
