import './bootstrap';

(() => {
    const elements = {
        calculateBtn: document.querySelector('.js-calculate'),
        currencyInput: document.querySelector('.js-currency-value'),
        rateList: document.querySelector('.js-rate-list'),
        select: document.querySelector('.js-select'),
        tableCalculationResults: document.querySelector('.js-table-calculation-results'),
        errorMessage: document.querySelector('.js-error-message'),
        calculations: document.querySelector('.js-calculations'),
        purchaseBtn: document.querySelector('.js-purchase'),
        successMessage: document.querySelector('.js-success-message')
    }

    if (elements.calculateBtn) {
        elements.calculateBtn.addEventListener('click', showCalculations);
    }

    if (elements.purchaseBtn) {
        elements.purchaseBtn.addEventListener('click', purchaseCalculations);
    }

    function getCalcValues() {
        const inputValue = elements.currencyInput && elements.currencyInput.value;
        const currencyId = elements.select && elements.select.value;

        return {
            currencyId: parseInt(currencyId),
            inputValue: parseFloat(inputValue)
        }
    }

    async function calc() {
        const url = '/api/orders/calculate';

        try {
            let res = await fetch(url, {
                method: 'POST',
                body: JSON.stringify({
                    currencyId: getCalcValues().currencyId,
                    amount: getCalcValues().inputValue
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                    'Accept': 'application/json'
                },
            });
            return await res.json();
        } catch (error) {
            console.log(error);
        }
    }

    async function showCalculations() {
        const listOfCalculations = await calc();
        const { data } = listOfCalculations;
        const { message } = listOfCalculations;

        if (elements.calculateBtn) elements.calculateBtn.setAttribute('disabled', 'disabled');
        if (elements.successMessage) elements.successMessage.classList.add('d-none');
        if (elements.errorMessage) elements.errorMessage.classList.add('d-none');
        if (elements.calculations) elements.calculations.classList.add('d-none');

        if (data) {
            let html = '';
            Object.values(data).forEach(d => {
                let htmlSegment = `<td>${d}</td>`;
                html += htmlSegment;
            });

            if (elements.tableCalculationResults) {
                elements.tableCalculationResults.innerHTML = html;

                if (elements.calculations) elements.calculations.className = '';
            }
        }

        if (message && elements.errorMessage) {
            elements.errorMessage.innerHTML = message;
            elements.errorMessage.classList.remove('d-none');
        }

        if (elements.calculateBtn) elements.calculateBtn.removeAttribute('disabled');
    }

    async function purchase() {
        const url = '/api/orders/create';

        try {
            let res = await fetch(url, {
                method: 'POST',
                body: JSON.stringify({
                    currencyId: getCalcValues().currencyId,
                    amount: getCalcValues().inputValue
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                    'Accept': 'application/json'
                },
            });
            return await res.json();
        } catch (error) {
            console.log(error);
        }
    }

    async function purchaseCalculations() {
        const listOfCalculations = await purchase();
        const { data } = listOfCalculations;

        if (data && data.id && elements.successMessage) {
            elements.successMessage.classList.remove('d-none');
            elements.successMessage.textContent = `Your order has been purchased. You can use this id (${data.id}) for reference.`;

            if (elements.calculations) elements.calculations.classList.add('d-none');
            if (elements.currencyInput) {
                elements.currencyInput.value = '';
                elements.currencyInput.focus();
            }
            if (elements.select) elements.select.value = 1;
        }
    }

    async function getData() {
        const url = '/api/currencies';
        try {
            let res = await fetch(url);
            return await res.json();
        } catch (error) {
            console.log(error);
        }
    }

    async function renderData() {
        const listOfCurrency = await getData();
        const { data } = listOfCurrency;

        let htmlRenderList = '';
        let htmlSelectRenderList = '';

        if (data) {
            data.forEach(currency => {
                let htmlSegment = `<li class="list-group-item d-flex justify-content-between align-items-center">${currency.name}<span class="text-dark">${currency.exchangeRate}</span></li>`;
                let htmlSelectSegment = `<option value="${currency.id}">${currency.name}</option>`;

                htmlRenderList += htmlSegment;
                htmlSelectRenderList += htmlSelectSegment;
            });

            if (elements.rateList) {
                elements.rateList.innerHTML = htmlRenderList;
            }

            if (elements.select) {
                elements.select.innerHTML = htmlSelectRenderList;
            }
        }
    }

    renderData();
})();
